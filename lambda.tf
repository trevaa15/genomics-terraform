resource "aws_lambda_function" "my_function" {
  function_name = "exifRemover"
  filename      = "files/exifRemover.zip"

  handler = "lambda_function.lambda_handler"
  runtime = "python3.9"

  role    = aws_iam_role.lambda_exec_role.arn
  timeout = "40"

}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "lambda_exec_role" {
  name               = "lambda_exif_remover_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_policy" "lambda_s3_policy" {
  name        = "lambda_exif_remover_policy"
  description = "Policy for lambda function to remove exif from jpeg files"

  policy = <<EOF

  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Sid": "VisualEditor0",
              "Effect": "Allow",
              "Action": [
                  "s3:CreateAccessPoint",
                  "s3:GetObjectVersionTagging",
                  "s3:ListBucketVersions",
                  "s3:GetBucketLogging",
                  "s3:RestoreObject",
                  "s3:CreateBucket",
                  "s3:ListBucket",
                  "s3:ReplicateObject",
                  "s3:GetObjectAcl",
                  "s3:GetObjectVersionAcl",
                  "s3:GetObjectTagging",
                  "s3:DeleteObject",
                  "s3:ListBucketMultipartUploads",
                  "s3:GetObjectRetention",
                  "s3:PutObjectLegalHold",
                  "s3:GetBucketAcl",
                  "s3:GetObjectLegalHold",
                  "s3:GetBucketNotification",
                  "s3:PutObject",
                  "s3:GetObject",
                  "s3:GetObjectTorrent",
                  "s3:DescribeJob",
                  "s3:PutObjectRetention",
                  "s3:GetBucketCORS",
                  "s3:GetObjectVersionForReplication",
                  "s3:GetBucketLocation",
                  "s3:ReplicateDelete",
                  "s3:GetObjectVersion"
              ],
              "Resource": [
                  "arn:aws:s3:::${aws_s3_bucket.s3_bucket_a.id}",
                  "arn:aws:s3:::${aws_s3_bucket.s3_bucket_b.id}",
                  "arn:aws:s3:::*/*",
                  "arn:aws:s3:*:${data.aws_caller_identity.current.account_id}:accesspoint/*",
                  "arn:aws:s3:*:${data.aws_caller_identity.current.account_id}:job/*"
              ]
          },
          {
              "Sid": "VisualEditor1",
              "Effect": "Allow",
              "Action": [
                  "s3:ListAllMyBuckets",
                  "s3:CreateJob"
              ],
              "Resource": "*"
          }
      ]
  }
EOF
}

resource "aws_iam_role_policy_attachment" "attach-policy2" {
  role       = aws_iam_role.lambda_exec_role.name
  policy_arn = aws_iam_policy.lambda_s3_policy.arn
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.my_function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_bucket_a.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_bucket_a.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.my_function.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
