resource "aws_s3_bucket" "s3_bucket_a" {
  bucket = "test-bucket-a-tf15"
  acl    = "private"
}


#defines the public access for the above bucket
resource "aws_s3_bucket_public_access_block" "s3_bucket_a_access" {
  bucket = aws_s3_bucket.s3_bucket_a.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


resource "aws_s3_bucket" "s3_bucket_b" {
  bucket = "test-bucket-b-tf15"
  acl    = "private"
}


#defines the public access for the above bucket
resource "aws_s3_bucket_public_access_block" "s3_bucket_b_access" {
  bucket = aws_s3_bucket.s3_bucket_a.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
