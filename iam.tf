# User A setup
resource "aws_iam_user" "S3_user_a" {
  name = "user-a"

}

resource "aws_iam_user_policy" "s3_user_a_policy" {
  name = "user-a-policy"
  user = aws_iam_user.S3_user_a.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:ChangePassword"
            ],
            "Resource": [
                "arn:aws:iam::*:user/${aws:username}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:GetAccountPasswordPolicy"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "user_a_access_key" {
  user = aws_iam_user.S3_user_a.name
}

resource "aws_iam_group" "S3_group_a" {
  name = "s3-group-a"
  path = "/users/"
}

resource "aws_iam_group_policy" "s3_a_group_policy" {
  name  = "s3-a-group-policy"
  group = aws_iam_group.S3_group_a.name

  policy = = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutAnalyticsConfiguration",
                "s3:PutAccessPointConfigurationForObjectLambda",
                "s3:GetObjectVersionTagging",
                "s3:DeleteAccessPoint",
                "s3:CreateBucket",
                "s3:DeleteAccessPointForObjectLambda",
                "s3:GetStorageLensConfigurationTagging",
                "s3:ReplicateObject",
                "s3:GetObjectAcl",
                "s3:GetBucketObjectLockConfiguration",
                "s3:DeleteBucketWebsite",
                "s3:GetIntelligentTieringConfiguration",
                "s3:PutLifecycleConfiguration",
                "s3:GetObjectVersionAcl",
                "s3:DeleteObject",
                "s3:GetBucketPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:PutReplicationConfiguration",
                "s3:PutObjectLegalHold",
                "s3:GetObjectLegalHold",
                "s3:GetBucketNotification",
                "s3:PutBucketCORS",
                "s3:DescribeMultiRegionAccessPointOperation",
                "s3:GetReplicationConfiguration",
                "s3:PutObject",
                "s3:GetObject",
                "s3:PutBucketNotification",
                "s3:DescribeJob",
                "s3:PutBucketLogging",
                "s3:GetAnalyticsConfiguration",
                "s3:PutBucketObjectLockConfiguration",
                "s3:GetObjectVersionForReplication",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetStorageLensDashboard",
                "s3:CreateAccessPoint",
                "s3:GetLifecycleConfiguration",
                "s3:GetInventoryConfiguration",
                "s3:GetBucketTagging",
                "s3:PutAccelerateConfiguration",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:DeleteObjectVersion",
                "s3:GetBucketLogging",
                "s3:RestoreObject",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:PutEncryptionConfiguration",
                "s3:GetEncryptionConfiguration",
                "s3:GetObjectVersionTorrent",
                "s3:AbortMultipartUpload",
                "s3:GetBucketRequestPayment",
                "s3:DeleteBucketOwnershipControls",
                "s3:GetAccessPointPolicyStatus",
                "s3:UpdateJobPriority",
                "s3:GetObjectTagging",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:DeleteBucket",
                "s3:PutBucketVersioning",
                "s3:GetBucketPublicAccessBlock",
                "s3:PutIntelligentTieringConfiguration",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:PutMetricsConfiguration",
                "s3:PutBucketOwnershipControls",
                "s3:UpdateJobStatus",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:PutInventoryConfiguration",
                "s3:GetObjectTorrent",
                "s3:GetStorageLensConfiguration",
                "s3:DeleteStorageLensConfiguration",
                "s3:PutBucketWebsite",
                "s3:PutBucketRequestPayment",
                "s3:PutObjectRetention",
                "s3:CreateAccessPointForObjectLambda",
                "s3:GetBucketCORS",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:ReplicateDelete",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:*:${aws_s3_bucket.s3_bucket_a.id}:storage-lens/*",
                "arn:aws:s3:::${aws_s3_bucket.s3_bucket_a.id}",
                "arn:aws:s3:us-west-2:${aws_s3_bucket.s3_bucket_a.id}:async-request/mrap/*/*",
                "arn:aws:s3:::*/*",
                "arn:aws:s3:*:${aws_s3_bucket.s3_bucket_a.id}:accesspoint/*",
                "arn:aws:s3:*:${aws_s3_bucket.s3_bucket_a.id}:job/*",
                "arn:aws:s3-object-lambda:*:${aws_s3_bucket.s3_bucket_a.id}:accesspoint/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:GetMultiRegionAccessPointPolicyStatus",
                "s3:GetMultiRegionAccessPointPolicy",
                "s3:GetMultiRegionAccessPoint",
                "s3:DeleteMultiRegionAccessPoint",
                "s3:CreateMultiRegionAccessPoint"
            ],
            "Resource": "arn:aws:s3::${aws_s3_bucket.s3_bucket_a.id}:accesspoint/*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": [
                "s3:GetAccessPoint",
                "s3:GetAccountPublicAccessBlock",
                "s3:PutStorageLensConfiguration",
                "s3:CreateJob"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_group_membership" "team_b" {
  name  = "s3-a-access"
  group = aws_iam_group.S3_group_a.name
  users = [
    aws_iam_user.S3_user_a.name,
  ]
}


# User B setup
resource "aws_iam_user" "S3_user_b" {
  name = "user-b"
}

resource "aws_iam_user_policy" "s3_user_b_policy" {
  name = "user-b-policy"
  user = aws_iam_user.S3_user_b.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:ChangePassword"
            ],
            "Resource": [
                "arn:aws:iam::*:user/${aws:username}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:GetAccountPasswordPolicy"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "user_b_access_key" {
  user = aws_iam_user.S3_user_b.name
}

resource "aws_iam_group" "S3_group_b" {
  name = "s3-group-b"
  path = "/users/"
}

resource "aws_iam_group_policy" "s3_b_group_policy" {
  name  = "s3-b-group-policy"
  group = aws_iam_group.S3_group_b.name

  policy = = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:GetLifecycleConfiguration",
                "s3:GetBucketTagging",
                "s3:GetInventoryConfiguration",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:GetObjectVersionTagging",
                "s3:GetBucketLogging",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:GetStorageLensConfigurationTagging",
                "s3:GetObjectVersionTorrent",
                "s3:GetObjectAcl",
                "s3:GetEncryptionConfiguration",
                "s3:GetBucketObjectLockConfiguration",
                "s3:GetIntelligentTieringConfiguration",
                "s3:GetBucketRequestPayment",
                "s3:GetAccessPointPolicyStatus",
                "s3:GetObjectVersionAcl",
                "s3:GetObjectTagging",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:GetBucketPublicAccessBlock",
                "s3:GetBucketPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetObjectLegalHold",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:GetBucketNotification",
                "s3:DescribeMultiRegionAccessPointOperation",
                "s3:GetReplicationConfiguration",
                "s3:GetObject",
                "s3:GetStorageLensConfiguration",
                "s3:GetObjectTorrent",
                "s3:DescribeJob",
                "s3:GetBucketCORS",
                "s3:GetAnalyticsConfiguration",
                "s3:GetObjectVersionForReplication",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetObjectVersion",
                "s3:GetStorageLensDashboard"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.s3_bucket_b.id}",
                "arn:aws:s3:*:${aws_s3_bucket.s3_bucket_b.id}:storage-lens/*",
                "arn:aws:s3:us-west-2:${aws_s3_bucket.s3_bucket_b.id}:async-request/mrap/*/*",
                "arn:aws:s3:::*/*",
                "arn:aws:s3:*:${aws_s3_bucket.s3_bucket_b.id}:accesspoint/*",
                "arn:aws:s3:*:${aws_s3_bucket.s3_bucket_b.id}:job/*",
                "arn:aws:s3-object-lambda:*:${aws_s3_bucket.s3_bucket_b.id}:accesspoint/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:GetMultiRegionAccessPointPolicyStatus",
                "s3:GetMultiRegionAccessPointPolicy",
                "s3:GetMultiRegionAccessPoint"
            ],
            "Resource": "arn:aws:s3::${aws_s3_bucket.s3_bucket_b.id}:accesspoint/*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": [
                "s3:GetAccessPoint",
                "s3:GetAccountPublicAccessBlock"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_group_membership" "team_b" {
  name  = "s3-b-access"
  group = aws_iam_group.S3_group_b.name
  users = [
    aws_iam_user.S3_user_b.name,
  ]
}
